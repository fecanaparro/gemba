//
//  QMVC.swift
//  GEMBA
//
//  Created by Felix Canaparro on 23/07/2018.
//  Copyright © 2018 AndroidFelixGravatai. All rights reserved.
//

import  Foundation
import UIKit
import MessageUI

class QMVC:UIViewController,MFMailComposeViewControllerDelegate,UIImagePickerControllerDelegate{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
        
        dateTxt.text=hoje()
        
        
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            scroll.contentInset = UIEdgeInsets.zero
        } else {
            scroll.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height, right: 0)
        }
        
        scroll.scrollIndicatorInsets = scroll.contentInset
        
    }
    
    
    
    
    //FUNCAO PARA OBTER A DATA DO DIA EM STRING
    func hoje () -> String{
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let myString = formatter.string(from: Date())
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd-MMM-yyyy"
        // again convert your date to string
        let myStringtxt = formatter.string(from: yourDate!)
        return(myStringtxt)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    func changeBtn(_ sender: UIButton!){
        switch sender.backgroundColor{
        case UIColor.white :
            sender.setTitle("Y", for: UIControlState.normal)
            sender.backgroundColor=UIColor.green
            sender.setTitleColor(UIColor.black, for: UIControlState.normal)
        case UIColor.green:
            sender.setTitle("N", for: UIControlState.normal)
            sender.backgroundColor=UIColor.red
            sender.setTitleColor(UIColor.white, for: UIControlState.normal)
        default:
            sender.setTitle(" ", for: UIControlState.normal)
            sender.backgroundColor=UIColor.white
            
        }
    }
    
    
    func generateGembaImage() -> UIImage {
        // Render view to an image
        UIGraphicsBeginImageContext(self.view.frame.size)
        view.drawHierarchy(in: self.view.frame, afterScreenUpdates: true)
        let gembaImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return gembaImage
    }
    @IBAction func shareGemba(_ sender: Any) {
        if MFMailComposeViewController.canSendMail() {
            let emailVC = MFMailComposeViewController()
            emailVC.mailComposeDelegate = self
            emailVC.setToRecipients(["fcanaparro@android-ind.com"])
            emailVC.setMessageBody("LPA PCL&Floor", isHTML:  false)
            emailVC.setSubject("Email from LPA App")
            emailVC.setCcRecipients(["lpagravatai@gmail.com"])
            //experiment//
            let image =  generateGembaImage()
            let imageString = returnEmailStringBase64EncodedImage(image: image)
            let emailBody = "<img src='data:image/png;base64,\(imageString)' width='\(image.size.width)' height='\(image.size.height)'>"
            //experiment
            emailVC.setMessageBody(emailBody, isHTML: true)
            present(emailVC, animated: true)
        } else {
            let ac = UIActivityViewController(activityItems: [generateGembaImage()], applicationActivities: nil)
            ac.completionWithItemsHandler = { activity, success, items, error in
            }
            let popOver = ac.popoverPresentationController
            popOver?.sourceView  = view
            popOver?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popOver?.permittedArrowDirections = UIPopoverArrowDirection.any
            present(ac, animated: true, completion: nil)
            
        }
    }
    //experiment//
    func returnEmailStringBase64EncodedImage(image:UIImage) -> String {
        let imgData:NSData = UIImagePNGRepresentation(image)! as NSData;
        let dataString = imgData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        return dataString
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    @IBOutlet weak var dateTxt: UITextField!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var segUnlock: UISegmentedControl!
    @IBOutlet weak var expTxtUm: UITextView!
    @IBOutlet weak var expTxtTres: UITextView!
    @IBOutlet weak var expTxtDois: UITextView!
    @IBOutlet weak var expTxtQuatro: UITextView!
    @IBOutlet weak var expTxtCinco: UITextView!
    @IBOutlet weak var expTxtSeis: UITextView!
    @IBOutlet weak var shareGemba: UIButton!
    @IBOutlet weak var piTxtUm: UITextView!
    @IBOutlet weak var piTxtDois: UITextView!
    @IBOutlet weak var piTxtCinco: UITextView!
    @IBOutlet weak var piTxtSeis: UITextView!
    @IBOutlet weak var piTxtTres: UITextView!
    @IBOutlet weak var piTxtQuatro: UITextView!
    @IBOutlet weak var checkTxtUm: UITextView!
    @IBOutlet weak var checkTxtDois: UITextView!
    @IBOutlet weak var checkTxtTres: UITextView!
    @IBOutlet weak var checkTxtQuatro: UITextView!
    @IBOutlet weak var checkTxtCinco: UITextView!
    @IBOutlet weak var checkTxtSeis: UITextView!
    @IBOutlet weak var leadTxtUm: UITextView!
    @IBOutlet weak var leadTxtTres: UITextView!
    @IBOutlet weak var leadTxtDois: UITextView!
    @IBOutlet weak var leadTxtCinco: UITextView!
    @IBOutlet weak var leadTxtQuatro: UITextView!
    @IBOutlet weak var leadTxtSeis: UITextView!
    
    
    
    @IBAction func tchBtn(_ sender: UIButton) {
        
        changeBtn(sender)
    }
    
    
    
    @IBAction func lock(_ sender: Any) {
        if segUnlock.selectedSegmentIndex==1
        {
            checkTxtUm.isEditable=false
            checkTxtUm.textColor=UIColor.black
            checkTxtDois.isEditable=false
            checkTxtDois.textColor=UIColor.black
            checkTxtTres.isEditable=false
            checkTxtTres.textColor=UIColor.black
            checkTxtQuatro.isEditable=false
            checkTxtQuatro.textColor=UIColor.black
            checkTxtCinco.isEditable=false
            checkTxtCinco.textColor=UIColor.black
            checkTxtSeis.isEditable=false
            checkTxtSeis.textColor=UIColor.black
            piTxtUm.isEditable=false
            piTxtUm.textColor=UIColor.black
            piTxtDois.isEditable=false
            piTxtDois.textColor=UIColor.black
            piTxtTres.isEditable=false
            piTxtTres.textColor=UIColor.black
            piTxtQuatro.isEditable=false
            piTxtQuatro.textColor=UIColor.black
            piTxtCinco.isEditable=false
            piTxtCinco.textColor=UIColor.black
            piTxtSeis.isEditable=false
            piTxtSeis.textColor=UIColor.black
            expTxtUm.isEditable=false
            expTxtUm.textColor=UIColor.black
            expTxtDois.isEditable=false
            expTxtDois.textColor=UIColor.black
            expTxtTres.isEditable=false
            expTxtTres.textColor=UIColor.black
            expTxtQuatro.isEditable=false
            expTxtQuatro.textColor=UIColor.black
            expTxtCinco.isEditable=false
            expTxtCinco.textColor=UIColor.black
            expTxtSeis.isEditable=false
            expTxtSeis.textColor=UIColor.black
            leadTxtUm.isEditable=false
            leadTxtUm.textColor=UIColor.black
            leadTxtDois.isEditable=false
            leadTxtDois.textColor=UIColor.black
            leadTxtTres.isEditable=false
            leadTxtTres.textColor=UIColor.black
            leadTxtQuatro.isEditable=false
            leadTxtQuatro.textColor=UIColor.black
            leadTxtCinco.isEditable=false
            leadTxtCinco.textColor=UIColor.black
            leadTxtSeis.isEditable=false
            leadTxtSeis.textColor=UIColor.black
            
        }else
        {
            checkTxtUm.isEditable=true
            checkTxtUm.textColor=UIColor.red
            checkTxtDois.isEditable=true
            checkTxtDois.textColor=UIColor.red
            checkTxtTres.isEditable=true
            checkTxtTres.textColor=UIColor.red
            checkTxtQuatro.isEditable=true
            checkTxtQuatro.textColor=UIColor.red
            checkTxtCinco.isEditable=true
            checkTxtCinco.textColor=UIColor.red
            checkTxtSeis.isEditable=true
            checkTxtSeis.textColor=UIColor.red
            piTxtUm.isEditable=true
            piTxtUm.textColor=UIColor.red
            piTxtDois.isEditable=true
            piTxtDois.textColor=UIColor.red
            piTxtTres.isEditable=true
            piTxtTres.textColor=UIColor.red
            piTxtQuatro.isEditable=true
            piTxtQuatro.textColor=UIColor.red
            piTxtCinco.isEditable=true
            piTxtCinco.textColor=UIColor.red
            piTxtSeis.isEditable=true
            piTxtSeis.textColor=UIColor.red
            expTxtUm.isEditable=true
            expTxtUm.textColor=UIColor.red
            expTxtDois.isEditable=true
            expTxtDois.textColor=UIColor.red
            expTxtTres.isEditable=true
            expTxtTres.textColor=UIColor.red
            expTxtQuatro.isEditable=true
            expTxtQuatro.textColor=UIColor.red
            expTxtCinco.isEditable=true
            expTxtCinco.textColor=UIColor.red
            expTxtSeis.isEditable=true
            expTxtSeis.textColor=UIColor.red
            leadTxtDois.isEditable=true
            leadTxtDois.textColor=UIColor.red
            leadTxtTres.isEditable=true
            leadTxtTres.textColor=UIColor.red
            leadTxtQuatro.isEditable=true
            leadTxtQuatro.textColor=UIColor.red
            leadTxtCinco.isEditable=true
            leadTxtCinco.textColor=UIColor.red
            leadTxtSeis.isEditable=true
            leadTxtSeis.textColor=UIColor.red
        }
        
    }
    
    
    
    
    
    
    
    
}
